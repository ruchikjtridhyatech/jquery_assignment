<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Rating Form(Jquery)</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="form">
		
			<table>
				<tr>
					<td class="label"><label>Your Rating :</label></td>
				
					<td>
					<input type="image" id="rating1" class="img1" src="img/empty.png" alt="img" height="50px" >
					<input type="image" id="rating2" class="img1" src="img/empty.png" alt="img" height="50px" >
					<input type="image" id="rating3" class="img1" src="img/empty.png" alt="img" height="50px" >
					<input type="image" id="rating4" class="img1" src="img/empty.png" alt="img" height="50px" >
					<input type="image" id="rating5" class="img1" src="img/empty.png" alt="img" height="50px" >
				</td>
			</tr>
			<tr class="review">
				<td ><label><p>Your Review :</p></label></td>
				<td><textarea></textarea></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" id="submit" value="Submit"></td>
			</tr>
			</table>
		
	</div>
</body>
</html>
<script src="jquery.js"></script>
<script>
	$(document).ready(function(){
		

		$(".img1").click(function(){
			var img=$(this).attr("src");
			$(this).prevUntil(".label").attr("src","img/full.png");
			if(img=="img/empty.png")
			{
				$(this).attr("src","img/half.png");
			}
			if(img=="img/half.png")
			{
				$(this).attr("src","img/full.png");	
			}
			if(img=="img/full.png")
			{
				$(this).attr("src","img/empty.png");
			}
			$(this).nextUntil(".label").attr("src","img/empty.png");

			var img_src=$("#rating1").attr("src");
			if(img_src=="img/empty.png")
			{
				$(".review").hide();
				$("textarea").val()='';
			}
			else
			{
				$(".review").show();
			}
		});


		$("#submit").click(function(){
			var img_src=$("#rating1").attr("src");
			if($("textarea").val()=='' || img_src=="img/empty.png")
			{
				alert("Please Give Rating ans Give Review!!");
			}
			else
			{
				var review=$("textarea").val();
				alert("Your Review : "+review);
			}
		});
		
	});
</script>