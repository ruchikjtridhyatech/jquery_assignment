<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Registration form(Jquery)</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<div class="form">
		<form accept-charset="utf-8" >
		<table class="tbl">
			<h2>Registration Form</h2>
			<tbody>
				<tr>
					<td>Full Name:</td>
					<td><input type="text" name="fname" id="fname" ></td>
					<td><p id="err_name">*Please Enter Your Name!</p></td>
				</tr>
				<tr>
					<td>Gender :</td>
					<td><label>Male<input type="radio" name="gender" id="gender" value="male" ></label>
						<label>Female<input type="radio" name="gender" id="gender" value="female"></label></td>
					<td><p id="err_gender">*Please select your gender!</p></td>
				</tr>
				<tr>
					<td>Hobby :</td>
					<td>
						<label>Dancing<input type="checkbox" name="hobby" id="hobby" class="dancing" value="dancing" ></label>
						<label>Singing<input type="checkbox" name="hobby" id="hobby" class="singing" value="singing" ></label>
						<label>Sports<input type="checkbox" name="hobby" id="hobby" class="sports" value="sports" ></label>
					</td>
					<td><p id="err_hoobie">*Please select any Hoobie!</p></td>
				</tr>
				<tr>
					<td>Phone Number: </td>
					<td><input type="number" name="num" id="num"  ></td>
					<td><p id="err_num">*Number is not valid</p></td>
				</tr>
				<tr>
					<td>Address :</td>
					<td><textarea name="address" id="address" ></textarea></td>
					<td><p id="err_address">*Please enter Adddress</p></td>
				</tr>
				<tr>
					<td>Stream :</td>
					<td><select name="stream" id="stream" >
						<option value="mca">MCA</option>
						<option value="mba">MBA</option>
						<option value="mcom">M.com</option>
					</select></td>
				</tr>
				<tr>
					<td>Email: </td>
					<td><input type="email" name="email" id="email" ></td>
					<td><p id="err_email">*email is not valid</p></td>
				</tr>
				<tr>
					<td>Password: </td>
					<td><input type="password" name="pass" placeholder="Require Capital, Special Char. and Digit & Min 8 letters" id="pass" ></td>
					<td><p id="err_pass1">Password is wrong</p></td>
				</tr>
				<tr>
					<td>Confirm Password: </td>
					<td><input type="password" name="pass2" id="pass2" ></td>
					<td><p id="err_pass2">*password does not matched</p></td>
				</tr>
				<tr>
					<td colspan="2"><input type="button"  name="btn" id="btn" value="Submit" class="btn"></td>
					<td style="opacity: 0;">Please Click On Submit</td>
				</tr>
			</tbody>
		</table>
		</form>
	</div>
</body>
</html>
<script src="jquery.js"></script>
<script>
	$(document).ready(function(){

		$("p").hide();
		var cnt_name=1;
		var cnt_num=1;
		var cnt_add=1;
		var cnt_email=1;
		var cnt_pass1=1;
		var cnt_pass2=1;

		// On Submit Buton
		$("#btn").click(function(){
			// debugger;
			// alert(cnt_add);
			if(cnt_name!=1 &&  cnt_num!=1 && cnt_add!=1 &&	cnt_email!=1 &&	cnt_pass1!=1 &&	cnt_pass2!=1)
			{
					var hobby = [];
		            $.each($("input[name='hobby']:checked"), function(){
		                hobby.push($(this).val());
		            });					
					var name=$("#fname").val();
					var num=$("#num").val();
					var gender=$("#gender:checked").val();
					var address=$("#address").val();
					var email=$("#email").val();
					var pass=$("#pass").val();

					alert(name+'\n'+num+'\n'+gender+'\n'+hobby+'\n'+address+'\n'+email+'\n'+pass);
					//alert("done");
			}
		
		});
	

		// On Name Textbox
		$("#fname").blur(function(){
			var name=$(this).val();
			if(name==''){
				$("#err_name").show();
				 cnt_name=1;
			}
		});
		$("#fname").focus(function(){
			 $("#err_name").hide();
			  cnt_name=0;
		});

		// On Phone number
		$("#num").blur(function(){
			var num=$(this).val();
			if(num==''){
				$("#err_num").show();
				cnt_num=1;
			}
			if(num.length != 10){
				$("#err_num").show();
				cnt_num=1;
			}
		});
		$("#num").focus(function(){
			 $("#err_num").hide();
			 cnt_num=0;
		});

		// On Address
		$("#address").blur(function(){
			var address=$(this).val();
			if(address==''){
				$("#err_address").show();
				cnt_add=1;
				// alert(cnt_add);
			}
		});
		$("#address").focus(function(){
			 $("#err_address").hide();
			 cnt_add=0;
			 // alert(cnt_add);
		});

		
		// On Email
		$("#email").blur(function(){
			var email=$(this).val();
			var email_len=email.split('.').reverse();
			if(email==''){
				$("#err_email").show();
				 cnt_email=1;
			}
			if(email_len[0].length > 3)  {
				$("#err_email").show();
				 cnt_email=1;
			}
			else
			{
				$("#err_email").hide();
				 cnt_email=0;
			}
		});
		$("#email").focus(function(){
			 $("#err_email").hide();
			  cnt_email=0;
		});

		// On Password Validation
		$("#pass").blur(function(){
			var pass=$(this).val();
			if(pass.length >=8)
			{
				if(pass.match(/[A-Z]/) && pass.match(/[0-9]/) && pass.match(/[&^%$#@!*]/))
				{
					var pass1=$(this).val();
					var pass2=$("#pass2").val();
						if(pass1!=pass2) // password matching
						{
							$("#err_pass2").show();
									 cnt_pass2=1;
						}
						else
						{

							$("#err_pass1").hide();
							cnt_pass1=0;
						}
				}
				else
				{
					$("#err_pass1").show();
							cnt_pass1=1;
				}
			}
			else
			{
				$("#err_pass1").show();
						 cnt_pass1=1;
			}
		});
		$("#pass").focus(function(){
			 $("#err_pass1").hide();
			 		 cnt_pass1=0;
		});

		// On Password Matching
		$("#pass2").blur(function(){
			var pass1=$(this).val();
			var pass2=$("#pass").val();
				if(pass1!=pass2) // password matching
				{
					$("#err_pass2").show();
							 cnt_pass2=1;
				}
		});
		$("#pass2").focus(function(){
			 $("#err_pass2").hide();
			  cnt_pass2=0;
		});
		
	});
</script>